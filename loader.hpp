#pragma once

#include <string>
#include <vector>

class Loader
{
public:
	void Init();
	void Inject(const std::string & process_name, std::vector<uint8_t>& dll, const std::string & jwt_token);
};

extern Loader loader;
