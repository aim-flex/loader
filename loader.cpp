#define _CRT_SECURE_NO_WARNINGS

#include "loader.hpp"

#include "network.hpp"
#include "injector.hpp"

#include "base64.h"

#include <stdio.h>
#include <tchar.h>

#include <Windows.h>
#include <atlbase.h>
#include <iostream>
#include <Winternl.h>
#pragma comment(lib, "ntdll.lib")
#include <fcntl.h>
#include <io.h>
#include <Psapi.h>
#include <tlhelp32.h>

#undef min
#undef max

#include "jwt/jwt.hpp"
#include "hwid.hpp"

// init runs on different thread than rendering
void Loader::Init()
{
	using namespace jwt::params;

	network.On("login_response", [](json& json)
	{
		try
		{
			std::string message = json.get<std::string>();

			auto dec_obj = jwt::decode(message, algorithms({ "ES256" }), verify(false), secret(ec_public_key));

			network.trace_handler(dec_obj.header().create_json_obj().dump());
			network.trace_handler(dec_obj.payload().create_json_obj().dump());

			network.jwt_token = message;
		}
		catch (const std::exception& e)
		{
			MessageBoxA(NULL, e.what(), e.what(), NULL);
		}
	});

	std::atomic<bool> driver_loaded = false;

	network.On("inject", [this, &driver_loaded](json& json)
	{
		std::string process_name = json[0];

		std::string decoded_dll = base64_decode(json[1]);
		std::vector<uint8_t> dll(decoded_dll.begin(), decoded_dll.end());

		//if (process_name == "csgo.exe")
		//	Inject(process_name, dll, network.jwt_token);
		//else
			InjectDriver(process_name, dll);
	});

	network.Connect("localhost", port);
	network.Send("login", json{ channel_loader, "admin", "password", hwid.Get() });

	while (true)
	{
		network.Think();
	}
}

void Loader::Inject(const std::string& process_name, std::vector<uint8_t>& dll, const std::string& jwt_token)
{
	injector.Inject(process_name, dll, jwt_token);
}

Loader loader;
