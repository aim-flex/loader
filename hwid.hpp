#pragma once

#include <string>

class HWID
{
public:
	const std::string Get();
};

extern HWID hwid;
