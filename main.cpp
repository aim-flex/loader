#include <windows.h>
#include <shellapi.h>
#include <commctrl.h>
#include <strsafe.h>
#include <thread>

#include "resource.h"

#define IDM_EXIT 100

#include "loader.hpp"
#include "injector.hpp"

LRESULT CALLBACK WndProc(HWND handle, UINT msg, WPARAM wparam, LPARAM lparam)
{
	NOTIFYICONDATA data = { sizeof(NOTIFYICONDATA) };

	switch (msg)
	{
		case WM_CREATE:
		{
			data.uID = 1;
			data.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;

			data.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1));

			data.hWnd = handle;
			data.uCallbackMessage = WM_USER + 1;
			Shell_NotifyIcon(NIM_ADD, &data);

			return 0;
		}
		case WM_DESTROY:
		{
			data.hWnd = handle;
			Shell_NotifyIcon(NIM_DELETE, &data);
			PostQuitMessage(0);

			return 0;
		}
		case WM_COMMAND:
		{
			if (LOWORD(wparam) == IDM_EXIT)
				PostQuitMessage(0);

			break;
		}
		case WM_USER + 1:
		{
			WORD cmd = LOWORD(lparam);

			if (cmd == WM_RBUTTONUP || cmd == WM_LBUTTONUP)
			{
				POINT pos;
				GetCursorPos(&pos);

				HMENU menu = CreatePopupMenu();
				InsertMenuA(menu, 0, MF_BYPOSITION | MF_STRING, IDM_EXIT, "Exit");

				SetForegroundWindow(handle);

				TrackPopupMenu(menu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN, pos.x, pos.y, 0, handle, NULL);
			}

			break;
		}
	}

	return DefWindowProc(handle, msg, wparam, lparam);
}

#include <io.h>
#include <fcntl.h>

int APIENTRY WinMain(HINSTANCE instance, HINSTANCE previnstance, char* cmdline, int cmdshow)
{
	/*
	AllocConsole();
	SetConsoleTitleA("ConsoleTitle");
	typedef struct { char* _ptr; int _cnt; char* _base; int _flag; int _file; int _charbuf; int _bufsiz; char* _tmpfname; } FILE_COMPLETE;
	*(FILE_COMPLETE*)stdout = *(FILE_COMPLETE*)_fdopen(_open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT), "w");
	*(FILE_COMPLETE*)stderr = *(FILE_COMPLETE*)_fdopen(_open_osfhandle((long)GetStdHandle(STD_ERROR_HANDLE), _O_TEXT), "w");
	*(FILE_COMPLETE*)stdin = *(FILE_COMPLETE*)_fdopen(_open_osfhandle((long)GetStdHandle(STD_INPUT_HANDLE), _O_TEXT), "r");
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	*/

	std::thread thread(&Loader::Init, loader);
	//thread.join();

	WNDCLASSEXW wx = { sizeof(WNDCLASSEX) };
	wx.lpfnWndProc = WndProc;
	wx.hInstance = instance;
	wx.lpszClassName = L"\uFEFF";
	RegisterClassExW(&wx);

	CreateWindowExW(0, L"\uFEFF", L"", 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, NULL, NULL);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
