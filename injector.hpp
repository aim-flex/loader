#pragma once

#include <vector>

class Injector
{
public:
	void Inject(const std::string& process_name, std::vector<uint8_t>& dll, const std::string& jwt_token);
};

extern Injector injector;
